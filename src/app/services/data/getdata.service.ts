import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetdataService {

  constructor(private http: HttpClient) { }

  getAccounts(token){
    const httpOptions = {
      headers: new HttpHeaders({
        authorization: "Bearer "+token
      })
    };
    
    return this.http
    .get('https://bhdleonfrontend-test.herokuapp.com/products/accounts', httpOptions)
    
  }

  getCards(token){
    const httpOptions = {
      headers: new HttpHeaders({
        authorization: "Bearer "+token
      })
    };
    
    return this.http
    .get('https://bhdleonfrontend-test.herokuapp.com/products/credit_cards', httpOptions)
  }
}