import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  loginBHD(id:string, pass:string){
    let body = {userId: id, password:pass}
    return this.http.post('https://bhdleonfrontend-test.herokuapp.com/sign_in', body);
  }
  isLogged(){
    let logged:any = localStorage.getItem('isLogged');
    if(logged){
      let accessToken = JSON.parse(logged)
      return {islogged: true, token: accessToken.token}
    }else{
      return {islogged: false}
    }
  }

}
