import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from "../../services/login/login.service";
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user:string;
  pass:string;

  constructor(
    private router: Router,
    public loginService : LoginService,
    public toastController: ToastController
  ) { }

  ngOnInit() {
    var logged = this.loginService.isLogged();
  }

  login(){
    
    this.loginService.loginBHD(this.user, this.pass).subscribe(
      (res:any) => {
        let logged = { 'token': res.access_token };
        localStorage.setItem('isLogged', JSON.stringify(logged));
        this.router.navigate(['/tab1']);
      },
      async (err) =>{
        const toast = await this.toastController.create({
          message: err.error.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
      }
    );
  }
  
}
