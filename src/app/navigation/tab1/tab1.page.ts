import { Component, OnInit } from '@angular/core';
import { LoginService } from "../../services/login/login.service";
import { GetdataService } from "../../services/data/getdata.service";

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})

export class Tab1Page implements OnInit {
  accounts : any;
  cards : any;
  aquan : number;
  cquan : number;
  token : string = this.loginService.isLogged().token;
  constructor(
    public loginService : LoginService,
    public getdataService: GetdataService
  ) { 
  }

  ngOnInit() {
    
    if(this.loginService.isLogged().islogged){
      this.getAccountsData();
      this.getCardsData();
    };
  }

  getAccountsData(){
    this.getdataService.getAccounts(this.token).subscribe(
      (res:any) => { 
        this.accounts = res;
        this.aquan = res.length;
      },
      (error) =>{
        console.error(error);
      }
    )
  }
  getCardsData(){
    this.getdataService.getCards(this.token).subscribe(
      (res:any) => { 
        console.log(res)
        this.cards = res;
        this.cquan = res.length;
      },
      (error) =>{
        console.error(error);
      }
    )
  }
}